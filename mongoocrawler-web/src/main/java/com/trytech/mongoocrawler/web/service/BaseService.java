package com.trytech.mongoocrawler.web.service;

import com.trytech.mongoocrawler.common.transport.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.web.netty.client.MonitorClientContext;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Created by coliza on 2018/9/16.
 */
public abstract class BaseService implements ApplicationContextAware {
    private static MonitorClientContext monitorClientContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (monitorClientContext == null) {
            monitorClientContext = MonitorClientContext.newInstance();
        }
    }

    protected <T extends AbstractProtocol> AbstractProtocol sendRequest(T t) {
        return monitorClientContext.sendRequest(t);
    }
}
