package com.trytech.mongoocrawler.web.controller;

import com.trytech.mongoocrawler.common.bean.ClientInfo;
import com.trytech.mongoocrawler.common.bean.MonitorData;
import com.trytech.mongoocrawler.common.bean.ServerConfig;
import com.trytech.mongoocrawler.common.transport.protocol.MonitorProtocol;
import com.trytech.mongoocrawler.web.service.ConfigService;
import com.trytech.mongoocrawler.web.vo.ServerConfVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collection;
import java.util.List;

/**
 * Created by coliza on 2018/6/2.
 */
@Controller
@RequestMapping("/config")
public class ConfigController extends BaseController {
    @Autowired
    private ConfigService configService;
    @RequestMapping("/server")
    public @ResponseBody
    ServerConfVO getConfig() {
        final ServerConfVO serverConfVO = new ServerConfVO();
        serverConfVO.setRunStatus("");
        serverConfVO.setMode(0);
        serverConfVO.setModeLabel("");
        serverConfVO.setCrawlerCount(1);

        try {
            MonitorData monitorData = configService.getConifg();
            ServerConfig serverConfig = monitorData.getServerConfig();
            serverConfVO.setMode(serverConfig.getMode());
            serverConfVO.setModeLabel(serverConfig.getModeLabel());
            serverConfVO.setCrawlerCount(serverConfig.getCrawlerCount());
            serverConfVO.setRunStatus(serverConfig.getRunStatusLabel());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return serverConfVO;
    }

    @RequestMapping("/clients")
    public
    @ResponseBody
    Collection<ClientInfo> getCrawlerClients() {
        List<ClientInfo> clientDataList = null;
        MonitorProtocol monitorProtocol = new MonitorProtocol();
        try {

            MonitorData monitorData = configService.getConifg();
            clientDataList = monitorData.getClientsConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clientDataList;
    }
}
