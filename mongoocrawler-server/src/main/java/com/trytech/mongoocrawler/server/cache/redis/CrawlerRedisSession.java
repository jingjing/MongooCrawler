package com.trytech.mongoocrawler.server.cache.redis;

import com.trytech.mongoocrawler.server.LocalCrawlerContext;
import com.trytech.mongoocrawler.server.xml.CrawlerXmlConfigBean;

/**
 * Created by coliza on 2017/4/9.
 */
public class CrawlerRedisSession
{
    private RedisClient redisClient;
    private String queue_key;
    /***
     * 构造函数

     * @param container 父容器
     * @param client
     */
    public CrawlerRedisSession(LocalCrawlerContext container, CrawlerXmlConfigBean crawlerXmlConfigBean, RedisClient client, String queue_key) {
        this.redisClient = client;
        this.queue_key = queue_key;
//        String[] startsUrls = new String[]{client.fetchFromQueue(queue_key)};
//        this.urlManager = new RedisUrlManager(this,redisClient);
//        this.urlManager.pushUrls(startsUrls);
    }

    public String getRedisKey(){
        return queue_key;
    }
}
