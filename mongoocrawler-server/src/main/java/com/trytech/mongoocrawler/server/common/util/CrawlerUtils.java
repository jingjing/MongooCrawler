package com.trytech.mongoocrawler.server.common.util;

import com.trytech.mongoocrawler.server.transport.http.CrawlHttpUrlParam;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by hp on 2017-1-24.
 */
public class CrawlerUtils {
    private final static HttpClient httpClient = new DefaultHttpClient();

    public static List<? extends NameValuePair> paramFromMap(Map<String, Object> params) {
        List paramList = new LinkedList();
        if (params != null) {
            for (String key : params.keySet()) {
                CrawlHttpUrlParam param = new CrawlHttpUrlParam(key, (String) params.get(key));
                paramList.add(param);
            }
        }
        return paramList;
    }
}
