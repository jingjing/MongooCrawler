package com.trytech.mongoocrawler.server.common.enums;

/**
 * Created by coliza on 2017/3/21.
 */
public enum CrawlerModeEnum {
    SINGLE(0, "单机模式"), DISTRIBUTE(1, "分布式模式");
    private int code;
    private String value;

    CrawlerModeEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public static String getModelLabel(int code) {
        switch (code) {
            case 0:
                return SINGLE.getValue();
            case 1:
                return DISTRIBUTE.getValue();
            default:
                return SINGLE.getValue();
        }
    }

    public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
