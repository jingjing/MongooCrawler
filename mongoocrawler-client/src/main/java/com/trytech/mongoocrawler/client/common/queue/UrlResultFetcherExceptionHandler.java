package com.trytech.mongoocrawler.client.common.queue;

import com.lmax.disruptor.ExceptionHandler;

/**
 * Created by coliza on 2017/4/15.
 */
public class UrlResultFetcherExceptionHandler implements ExceptionHandler<UrlResultFetcherEvent> {
    @Override
    public void handleEventException(Throwable ex, long sequence, UrlResultFetcherEvent event) {
        System.out.println(ex.getStackTrace());
        System.out.println(event.getData());
    }

    @Override
    public void handleOnStartException(Throwable ex) {

    }

    @Override
    public void handleOnShutdownException(Throwable ex) {

    }
}
