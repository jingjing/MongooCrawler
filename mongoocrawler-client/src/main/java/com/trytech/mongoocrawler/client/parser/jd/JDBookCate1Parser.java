package com.trytech.mongoocrawler.client.parser.jd;

import com.trytech.mongoocrawler.client.common.queue.UrlFetcherEventProducer;
import com.trytech.mongoocrawler.client.parser.HtmlParser;
import com.trytech.mongoocrawler.client.transport.http.UrlResult;
import com.trytech.mongoocrawler.client.transport.http.WebResult;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.nio.charset.Charset;
import java.util.Iterator;

/**
 * 京东商品页面解析器
 */
public class JDBookCate1Parser extends HtmlParser<Boolean> {

    @Override
    public Boolean parse(WebResult webResult, UrlFetcherEventProducer urlProducer) {
        try {
            String html = ((WebResult<String>)webResult).getData();
            Document doc = Jsoup.parse(html);
            doc.charset(Charset.forName("UTF-8"));
            Element body = doc.body();
            //获取包含图书种类的div
            Element bookeCatEle = body.getElementById("p-category");
            Element menuEle = bookeCatEle.getElementsByClass("menu").first();
            Elements itemEles = menuEle.getElementsByClass("item");

            for (Iterator ite = itemEles.iterator(); ite.hasNext(); ) {
                Element ele = (Element) ite.next();
                Element categoryEle = ele.getElementsByTag("a").first();
                String cate_url = categoryEle.attr("href");
                if(StringUtils.isEmpty(cate_url)){
                    continue;
                }
                if (StringUtils.isNotEmpty(cate_url)) {
                    urlProducer.sendData(new UrlResult("https:" + cate_url, new JDBookCate2Parser()));
                }
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
