package com.trytech.mongoocrawler.client.common.queue;

import com.trytech.mongoocrawler.client.transport.http.UrlResult;

/**
 *
 */
public class UrlResultFetcherEvent extends FetcherEvent<UrlResult> {
    @Override
    public UrlResult getData() {
        return this.data;
    }

    @Override
    public void setData(UrlResult data) {
        this.data = data;
    }
}
